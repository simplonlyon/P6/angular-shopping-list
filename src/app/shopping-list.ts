import { Product } from "./product";

export interface ShoppingList {
    id?: number;
    name: string;
    products?: Product[];
}
