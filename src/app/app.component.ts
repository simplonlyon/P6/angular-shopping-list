import { Component } from '@angular/core';

@Component({
  selector: 'app-root', // selector indique à angular quel est le nom de la balise du component.
  templateUrl: './app.component.html', // on précise à angular où se trouve le template du component.
  styleUrls: ['./app.component.css'] // on précise à angular où se trouve les styles du component.
})
export class AppComponent {
}
